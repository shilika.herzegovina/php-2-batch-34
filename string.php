<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1> Soal String </h1>
    <?php 
    echo "<h3> Soal No 1</h3>";
        $first_sentence = "Hello PHP!" ;
        $second_sentence = "I'm ready for the challenges";
        echo "First_Sentence = " . $first_sentence . "<br>";
        echo "Panjang String : = " . strlen ($first_sentence) . "<br>";
        echo "Jumlah Kata : = " . str_word_count($first_sentence) . "<br> <br>";

        echo "Second_Sentence = " . $second_sentence . "<br>";
        echo "Panjang String : = " . strlen ($second_sentence) . "<br>";
        echo "Jumlah Kata : = " . str_word_count($second_sentence) . "<br> <br>";
    
    echo "<h3> Soal No 2</h3>";
    $string2 = "I love PHP";
        
    echo "<label>String: </label> \"$string2\" <br>";
    echo "Kata pertama: " . substr($string2, 0, 1) . "<br>" ; 
    // Lanjutkan di bawah ini
    echo "Kata kedua: " . substr($string2,2,4) . "<br>";
    echo "Kata ketiga: " . substr($string2,7,3) ."<br> <br>";
    
    echo "<h3> Soal No 3 </h3>";
    $string3 = "PHP is old but sexy!";
        echo "Kalimat = " . $string3 . "<br>";
        echo "Kalimat di ganti = " . str_replace ("sexy","awesome",$string3);


    ?>
</body>
</html>